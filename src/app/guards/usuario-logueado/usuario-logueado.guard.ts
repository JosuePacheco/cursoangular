import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanDeactivate, CanLoad,  UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioLogueadoGuard implements CanActivate {

  constructor(private authService:AuthService,private router: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean>  | Promise<boolean> | boolean | UrlTree {
    const isLoggedIn= this.authService.isLoggedIn();
    console.log('canActivate', isLoggedIn);
    if( isLoggedIn) return isLoggedIn ;
    return this.router.createUrlTree(['/home']);
  }
  
}
