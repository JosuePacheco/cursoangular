import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, APP_INITIALIZER, Injectable } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesState, reducer, initilizeDestinosViajesState, DestinosViajesEffects } from './models/destinos-viajes-state.model';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import {  routes } from './app.routing.module';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ApiService } from './components/api.service';
import Dexie from 'dexie';
import { DestinoViaje } from './models/destino-viaje.model';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {HttpLoaderFactory} from '../app/models/i18n';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { RouterModule } from '@angular/router';
import { TrackearClickDirective } from './trackear-click.directive';


//app config
export interface AppConfig {
  apiEndpoint: string;
}
export const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
}
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//fin appconfig



//redux init
export interface AppState {
  destinos: DestinosViajesState;
}
const reducers: ActionReducerMap<AppState> = {
  destinos: reducer
}

const reducersInitialState = {
  destinos: initilizeDestinosViajesState()
}

//app init
export function init_app(appLoadService: ApiService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajes();
}

//redux fin init


//dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) { }
}

@Injectable({ providedIn: 'root' })
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;

  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id,nombre,imagenUrl'
    });
    this.version(2).stores({
      destinos: '++id,nombre,imagenUrl',
      translations: '++id,lang,key,value'

    });
  }
}

export const db = new MyDatabase();
//fin dexie


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    DestinoDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxMapboxGLModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, {
      initialState: reducersInitialState, runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false
      }
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide:TranslateLoader,
        useFactory:(HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    

  ],
  providers: [AuthService,MyDatabase, UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    ApiService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [ApiService], multi: true }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
