import { Injectable, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { nuevoDestinoAction, elegidoFavoritoAction } from './destinos-viajes-state.model';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinoApiClient {

destinos:DestinoViaje[]=[];
constructor(private store:Store<AppState>,
    @Inject(forwardRef(()=>APP_CONFIG))private config:AppConfig,
    private http:HttpClient)
    
    {
}

add(d:DestinoViaje){
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const req = new HttpRequest('POST',this.config.apiEndpoint+'/my',{nuevo:d.nombre},{headers:headers});
    this.http.request(req).subscribe((data:HttpResponse<{}>)=>{
        if(data.status ==200)
             this.store.dispatch(nuevoDestinoAction({destino:d}));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db');
        myDb.destinos.toArray().then(destinos => console.log(destinos));
    });
}

elegir(d:DestinoViaje){
    this.store.dispatch(elegidoFavoritoAction({destino:d}));
}

getById(id){
    return this.destinos.filter(d=>{return d.toString() === id;})[0];
}
}