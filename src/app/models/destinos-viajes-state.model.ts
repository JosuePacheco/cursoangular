import { DestinoViaje } from './destino-viaje.model';
import {createAction, props,createReducer,on, State,Action} from '@ngrx/store';
import {Actions,Effect,ofType, createEffect} from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';





export interface DestinosViajesState{
    items: DestinoViaje[];
    loading:boolean;
    favorito: DestinoViaje;
}

export const initilizeDestinosViajesState = ()=>{
    return {
        items: [],
        loading:false,
        favorito:null
    }
}


//acciones
export const nuevoDestinoAction = createAction('[Destinos Viajes] Nuevo',props<{destino:DestinoViaje}>());
export const elegidoFavoritoAction = createAction('[Destinos Viajes] Favorito',props<{destino:DestinoViaje}>());
export const voteUp =createAction('[Destinos Viajes] Vote Up',props<{destino:DestinoViaje}>());
export const voteDown =createAction('[Destinos Viajes] Vote Down',props<{destino:DestinoViaje}>());
export const InitiMyDataAction =createAction('[Destinos Viajes] InitiMyDataAction',props<{destinos:string[]}>());
//reducers

const reducerDestinoViajes = createReducer(
    initilizeDestinosViajesState(),
    on(nuevoDestinoAction,(state,{destino})=>({...state,items:[...state.items,destino]})),
    on(elegidoFavoritoAction,(state,{destino})=>{
            
            state.items.forEach(x => x.setSelected(false));
            let fav: DestinoViaje = destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito:fav,
            };
    }),
    on(voteUp,(state,{destino})=>{
        destino.voteUp();
        return{ ...state };
    }),
    on(voteDown,(state,{destino})=>{
        destino.voteDown();
        return{ ...state };
    }),
    on(InitiMyDataAction,(state,{destinos})=>{
        if(!destinos)
            destinos =[];
    return {
        ...state,
        items:destinos.map((d)=>new DestinoViaje(d,''))
    }
    })
);

export function reducer(state:DestinosViajesState | undefined,action:Action){
    return reducerDestinoViajes(state,action);
}

//efects
@Injectable()
export class DestinosViajesEffects{
    
    nuevoAgregado$ = createEffect(()=> this.actions$.pipe(
        ofType(nuevoDestinoAction),
        map(action => elegidoFavoritoAction({destino:action.destino}))
    ));
    
    constructor(private actions$: Actions){}
}