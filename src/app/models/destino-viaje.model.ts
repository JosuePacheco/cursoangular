
import { v4 as uuid } from 'uuid';

export class DestinoViaje {

    public selected: boolean = false;
    servicios: string[];
    id = uuid();
    constructor(public nombre: string, public url: string,public votes:number=0) {
        this.servicios = ['pileta', 'desayuno'];
    }

    isSelected(): boolean {
        return this.selected;
    }

    public setSelected(s: boolean) {
        this.selected = s;
    }
    voteUp() {
       this.votes++;
    }

    voteDown(){
        this.votes--;
    }
}
