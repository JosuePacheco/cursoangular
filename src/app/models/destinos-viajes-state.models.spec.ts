import {reducer,
DestinosViajesState,
initilizeDestinosViajesState,
InitiMyDataAction,
nuevoDestinoAction} from './destinos-viajes-state.model';
import {DestinoViaje} from './destino-viaje.model';

describe('reducerDestinosViajes',()=>{
    it('sholud reduce init data', ()=>{
        const prevState:DestinosViajesState = initilizeDestinosViajesState();
        const action  = InitiMyDataAction({destinos:['destino 1', 'destino 1']});
        const newState: DestinosViajesState = reducer(prevState,action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item added',()=>{
        const prevState:DestinosViajesState = initilizeDestinosViajesState();
        const action  = nuevoDestinoAction({destino: new DestinoViaje('barcelona','url')});
        const newState: DestinosViajesState = reducer(prevState,action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    })

})