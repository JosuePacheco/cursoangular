import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinoApiClient } from './../../models/destinos-api.client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinoApiClient]
})
export class ListaDestinosComponent implements OnInit {

  updates:string[];
  destinos: DestinoViaje[]=[];
  all;
  constructor(private destinosApiClient:DestinoApiClient, private store:Store<AppState>) {
    this.updates =[];

    this.store.select(state => state.destinos.favorito)
              .subscribe(data => {
                  const d = data;
                  if(d!= null){
                    this.updates.push('Se eligio: ' + d.nombre);  
                  }
              });
      store.select(state => state.destinos.items).subscribe(items => this.all = items);

  }

  ngOnInit(): void {
  }

  agregado(d:DestinoViaje){
    this.destinosApiClient.add(d);
  }

  elegido(e:DestinoViaje){
    this.destinosApiClient.elegir(e);
  }

  getAll(){

  }

}
