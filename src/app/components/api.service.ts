import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG_VALUE } from '../app.module';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { InitiMyDataAction } from '../models/destinos-viajes-state.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private store:Store<AppState>, private http:HttpClient) { }

  async initializeDestinosViajes():Promise<any>{
    const headers :HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const req = new HttpRequest('GET',APP_CONFIG_VALUE.apiEndpoint + '/my',{headers:headers});
    const response :any = await this.http.request(req).toPromise();
    this.store.dispatch(InitiMyDataAction({destinos:response.body}));
  }
}
